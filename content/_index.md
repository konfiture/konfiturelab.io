---
title: Konfiture
---

# What is it ?

Konfiture is a speller and grammar checker for markdown files.

Give konfiture one or several markdown files and it will print the file with the unkwown words and also the grammar errors with some corrections.

```
$ konfiture file0.md [file1.md...]
```

Konfiture only support **french**, why you ask ? Because I am french, so you might wonder why the documentation is written english.
The reason is simple I plan to support english, I just haven't found a good open source grammar and speller checker.
If you know one I will be more than happy to integrate it into konfiture.

> FYI I haven't coded the french checker, I have just integrated [Grammalecte](http://grammalecte.net).

# How to install it ?

Konfiture is coded in python, so it is available on pip.

```
$ pip3 install konfiture
```

# Is there more ?

Nope there is not, you know everything there is to know about Konfiture.

# Hope you will enjoy it ...

<style>
section.landing article h4{
  text-align: left;
}
</style>

> #### Pastry fact
> Konfiture is the french word for jam, in fact the real word is **confiture**, not konfiture.
> I find quite funny the fact that a speller checker have a name which is mispelled.
> I am not sure there much things to say about jam. So I will give you a recipe to make a great Jam. The secret is juicy fruits with sugar, sugar and more sugar.
> Happy coding, happy cooking.
